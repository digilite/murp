<footer itemscope itemtype="http://schema.org/WPFooter">
		    <div class="container">
				<img itemprop="logo" src="<?php echo get_template_directory_uri(); ?>/img/Logo_white.png">
				<div class="row contact-info">
					<div class="col-md-4 col-12">
						<p>
						Two Morneau Shepell Centre<br>
						895 Don Mills Road, Suite 900<br>
						Toronto, Ontario<br>
						M3C 1W3, Canada<br><br>
						
						</p>
					</div>
					<div class="col-md-4 col-12">
						<p>Hours: Mon-Sun 9:00AM - 6:00PM</p>
						<p><i class="fas fa-phone"></i>647-615-2884</p>
						<p><i class="far fa-envelope"></i>info@manageyourproperty.com</p>
					</div>
					<div class="col-md-4 col-12">
						<div class="social-icons flex-container">
                            <a href="https://www.facebook.com/Manage-Your-Property-111162394043969"><i class="fab fa-facebook-f"></i></a>
							<a href="https://www.instagram.com/manageyourproperty/"><i class="fab fa-instagram"></i></a>
							<a href="https://www.youtube.com/channel/UCh4Y7KrHsBAciZPuBKMgaNw?view_as=subscriber"><i class="fab fa-youtube"></i></a>
						</div>
						<p class="copyright">All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); ?>
		<div class="mobile-nav">
			<div class="container">
				<div class="close">
					<p>X</p>
				</div>
			</div>
		</div>
	</body>
</html>