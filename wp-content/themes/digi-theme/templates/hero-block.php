<?php $hero_type = get_field("choose_hero_type");
if(get_field("choose_hero_type")) : $hero_c = "type-1"; 
else : $bg = "background: linear-gradient(90deg, rgba(10,7,61,0.3393732492997199) 0%, rgba(0,0,0,0.3393732492997199) 50%, rgba(0,0,0,0.34217436974789917) 100%), url(".get_the_post_thumbnail_url().") no-repeat center center / cover"; $hero_c = "type_2";
endif;
$bgvideo = get_field("hero_video"); ?>
<section class="hero hero_<?= $hero_c; ?>" style="<?= $bg; ?>">
    <?php if(get_field("choose_hero_type")) : ?>
    <div id="player" data-src="https://www.youtube.com/embed/O6CLkUKePm8"></div>
    <script>
        // 1. Get the youtube video ID.
        video_container = document.getElementById('player')
        var youtube_src = video_container.getAttribute("data-src");
        var src_split = youtube_src.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        var id = (src_split[2] !== undefined) ? src_split[2].split(/[^0-9a-z_\-]/i)[0] : src_split[0];

        // 2. This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
            videoId: id,
            playerVars: {
                playlist: id,
                autoplay: 1,        // Auto-play the video on load
                autohide: 0,
                disablekb: 1, 
                controls: 1,        // Hide pause/play buttons in player
                showinfo: 0,        // Hide the video title
                modestbranding: 1,  // Hide the Youtube Logo
                loop: 1,            // Run the video in a loop
                fs: 0,              // Hide the full screen button
                autohide: 0,         // Hide video controls when playing
                rel: 0,
                enablejsapi: 1
            },
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
            });
        }

        // 4. The API will call this function when the video player is ready.
        function onPlayerReady(event) {
            event.target.mute()
            event.target.playVideo();
        }

        // 5. The API calls this function when the player's state changes.
        //    The function indicates that when playing a video (state=1),
        //    the player should play for six seconds and then stop.
        var done = false;
        function onPlayerStateChange(event) {
            //event.target.playVideo();
        }
        function stopVideo() {
            //player.stopVideo();
        }
    </script>

    <div class="container">
        <h1 class="hero-title"><?php the_field("hero_title"); ?></h1>
    </div>
    <?php else : ?>
    <h1 class="text-center page-title"><?php the_title(); ?></h1>
    <div id="triangle"></div>
    <?php endif; ?>
</section>