<?php 
/*
     Template Name: Blog
*/

get_header(); 
require_once("templates/hero-block.php");

?>

<section class="blog-in">
	<div class="container">
		<div class="row blog-box">
			<?php 
				$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$params = array(
					'posts_per_page' => 4,
					'post_type'       => 'post',
					'category_name' => 'blog',
					'paged'           => get_query_var('paged') ?: 1
				);
				query_posts($params);
				
				$wp_query->is_archive = true;
				$wp_query->is_home = false;
				
				while(have_posts()): the_post();
			?>

			<div class="col-md-6 blog-container">
				<div class="blog-item d-flex flex-column">
					<div class="post-info post-date d-flex justify-content-between">
						<date><?php echo get_the_date(); ?></date>
						<div class=" post-social-icons text-right">
							<?php echo do_shortcode('[share]'); ?>
						</div>
					</div>
					<a class="absolute" href="<?php the_permalink(); ?>"></a>
					<div class="thumb text-center">
						<?php echo get_the_post_thumbnail( get_the_ID(), 'blog-thumb', array("class"=>"img-fluid") ); ?>
					</div>
					<h2 class="post-title"><?php the_title(); ?></h2>
					<p class="post-desc"><?php the_excerpt(); ?></p>
					<span class="read-more text-right">Read More</span>
				</div>
			</div>

			<?php endwhile; ?>
		</div>
		<nav class="post-navigation">
			<?php previous_posts_link( 'Previous' ); ?>
			<?php next_posts_link('Next') ?>
			<span class="num-pages">You are on page <?php echo $current_page; ?> of
			<?php echo $wp_query->max_num_pages; ?></span>
		</nav>
		<?php wp_reset_query(); ?>
	</div>
</section>

<?php get_footer(); ?>