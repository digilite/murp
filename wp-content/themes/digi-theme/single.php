<?php get_header(); ?>

<section class="single-post-in">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-12">
			    <div class="thumb text-center">
				   <div class="dark"></div>
				   <?php echo get_the_post_thumbnail( get_the_ID(), 'large' ); ?>
				   <h2 class="single-post-title text-left"><?php the_title() ?></h2>
				   <div class="post-info flex-container">
						<div class="col-md-6 col-6 post-date text-left">
							<date><?php echo get_the_date(); ?></date>
						</div>
						<div class="col-md-6 col-6 post-date post-social-icons text-right">
						    <?php echo do_shortcode('[share]'); ?>
						</div>
		            </div>
				</div>
				<div class="user-content single-post-content">
					<p><?php the_content(); ?></p>
				</div>
			</div>
			<?php $params = array(
					'posts_per_page' => 3,
					'post_type'       => 'post',
					'category_name' => 'blog',
					'post__not_in' => array(get_the_ID()),
				);
				query_posts($params);
				$wp_query->is_archive = true;
				$wp_query->is_home = false;
				if (have_posts()): 
			?>

			<div class="col-md-4 col-12">
			    <p class="recent-block-title">Latest Articles</p>
				<?php while(have_posts()): the_post(); ?>
                    <div class="flex-container recent-item">
						<div class="recent-thumb col-md-4">
							<img src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="thumbnail">
						</div>
						<div class="recent-title col-md-8">
							<a href="<?php the_permalink(); ?>" class="recent-post-title"><?php the_title(); ?></a>
							<date><?php echo get_the_date(); ?></date>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
			<?php endif; wp_reset_query(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>