<?php 

/*
     Template Name: Services
*/

get_header(); 


require_once("templates/hero-block.php");

?>

<section class="services-in">
    <div class="container">
		<div class="row">
		    <div class="col-md-3"></div>
			<div class="col-md-9">
				<p class="hw-in-g-title services-in-title">Sign up with Manage Your Property in 5 easy steps:</p>
			</div>
			<?php if( have_rows('services_in') ):
			while( have_rows('services_in') ) : the_row();
			$stitle = get_sub_field('servces_list_title_in');
			$sicon = get_sub_field('services_icon_in'); ?>
				<div class="col-md-3 text-center services-icon">
					<div class="s-icon">
						<img src="<?= $sicon; ?>" alt="calendar-icon">
					</div>
				</div>
				<div class="col-md-9 s-box">
					<p class="services-list-title"><?= $stitle; ?></p>
					<?php if( have_rows('services_list_in') ): ?>
					<ul class="services-list">
						<?php while( have_rows('services_list_in') ): the_row(); ?>
							<li><?php the_sub_field('services_list_item_in'); ?></li>
					<?php endwhile; ?>
					</ul>
					<?php endif; ?>
				</div>
			<?php 
			endwhile;
			else :
			endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>