<?php 

/*
     Template Name: Testimonials
*/

get_header(); 


?>



<section class="testimonials-in" style="background: linear-gradient(90deg, rgb(0 0 0 / 0.75) 0%, rgb(0 0 0 / 0.75) 50%, rgb(0 0 0 / 0.75) 100%), url(<?php the_field("test_background_image"); ?>) no-repeat center center / cover">
    <div class="container">
		<div class="row">
		   <h1 class="text-center page-title in-page-title"><?php the_title(); ?></h1>
		   <div class="owl-carousel owl-theme testimonial-item text-center">
			<?php 
				$params = array(
					'post_type'       => 'testimonials'
				);
				query_posts($params);
				while(have_posts()): the_post(); 
				$thumbnail = get_the_post_thumbnail_url(get_the_ID());
			?>
					<div class="item">
						<?php if(!empty($thumbnail)): ?>
							<img class="testimonial-thumb" src="<?= $thumbnail; ?>" alt="thumbnail">
						<?php endif; ?>
						<h2 class="auth-name"><?php the_title(); ?></h2>
						<p class="testimonial-desc"><?php the_content(); ?></p>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>