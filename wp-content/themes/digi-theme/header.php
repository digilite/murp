<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo wp_get_document_title(); ?></title>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400&display=swap" rel="stylesheet">
	<!-- Generate favicon here http://www.favicon-generator.org/ -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo("template_url") ?>/img/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo("template_url") ?>/img/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("template_url") ?>/img/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo("template_url") ?>/img/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("template_url") ?>/img/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo("template_url") ?>/img/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo("template_url") ?>/img/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo("template_url") ?>/img/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("template_url") ?>/img/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo("template_url") ?>/img/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo("template_url") ?>/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo("template_url") ?>/img/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo("template_url") ?>/img/favicon-16x16.png">
	<link rel="manifest" href="<?php bloginfo("template_url") ?>/img/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php if(get_field("choose_hero_type")) : $bgvideo = "hero-video"; else: $bgvideo = "hero-image"; endif; ?>
	<header class="none-scrolled <?= $bgvideo; ?>" itemscope itemtype="http://schema.org/WPHeader">
		<div class="container flex-container header-items">
			<div class="col-lg-4 col-5" itemscope itemtype="http://schema.org/Organization" id="logo">
				<a class="logo" itemprop="url" href="<?php echo bloginfo('url') ?>">
					<?php echo file_get_contents(get_field("logo","option")); ?>
				</a>
			</div>
			<nav class="primary-menu col-lg-8 col-7 navbar-expand-lg d-flex align-items-center justify-content-end">
				<div class="mobile-menu navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<div id="burger-icon"><span></span><span></span><span></span><span></span><span></span><span></span></div>
				</div>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<?php wp_nav_menu (array("theme_location" => "primary-menu", "container" => "", "menu_class" => "main-menu"));?>
				</div>
			</nav>
		</div>
	</header>