<?php 
/*
     Template Name: Front Page
*/
get_header(); 
require_once("templates/hero-block.php");
?>
<section class="about-company">
	<div class="container">
		<div class="about-description text-center">
			<h2 class="ab-title"><?= the_field('fr_aboutcompany_title'); ?></h2>
			<img itemprop="line" src="<?php echo get_template_directory_uri(); ?>/img/line_02.png">
			<p class="ab-description"><?= the_field('fr_aboutcompany_description'); ?></p>
		</div>
	</div>
</section>
<section class="let-us">
	<div class="container">
		<?php if( have_rows('fr_letsus_block') ):
    while( have_rows('fr_letsus_block') ) : the_row();
	$title = get_sub_field('fr_lets_us_title');
	$description = get_sub_field('fr_letsus_description'); ?>
		<div class="lt-manage-property">
			<h3 class="p-title"><?= $title; ?></h3>
			<?= $description; ?>
		</div>
		<?php endwhile;
    else :
    endif; ?>
	</div>
</section>
<section class="what-sets sets"
	style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/sets-bg.jpg);">
	<div class="container">
		<h3 class="wh-title p-title">What Sets Us Apart</h3>
		<?php if( have_rows('fr_sets_item') ):
			while( have_rows('fr_sets_item') ) : the_row();
			$wtitle = get_sub_field('fr_sets_title');
			$wdescription = get_sub_field('fr_sets_description');
			$wicon = get_sub_field('fr_sets_icon'); ?>
		<div class="wh-items row">
			<div class="col-lg-1 col-md-2">
				<div class="wh-icon text-center">
					<img src="<?= $wicon; ?>" alt="">
				</div>
			</div>
			<div class="col-lg-11 col-md-10">
				<h4 class="wh-in-title p-title"><?= $wtitle; ?></h4>
				<?= $wdescription; ?>
			</div>
		</div>
		<?php endwhile;
			else :
			endif; ?>
	</div>
</section>
<section class="how-it-works">
	<div class="container">
		<h2 class="ab-title text-center">How it works</h2>
		<img class="line" itemprop="line" src="<?php echo get_template_directory_uri(); ?>/img/line_02.png">
		<div class="video-block">
			<div class="row justify-content-center">
				<div class="col-md-7">
					<?php 
						if($video = get_field("video")):
							echo $video; 
						endif;
					?>
				</div>
			</div>		
		</div>
		<div class="row hw-list">
			<?php if( have_rows('fr_how_it_works') ):
			$count = 1;
			while( have_rows('fr_how_it_works') ) : the_row();
			$hwtitle = get_sub_field('fr_hw_item_text'); ?>
			<div class="col-md-6 col-12">
				<div class="row">
					<div class="col-md-2 col-2 m-auto">
						<p class="hw-number"><?= $count; ?></p>
					</div>
					<div class="col-md-10 col-10 m-auto">
						<p class="hw-description"><?= $hwtitle; ?></p>
					</div>
				</div>
			</div>
			<?php $count++;
			endwhile;
			else :
			endif; ?>
		</div>
		<a href="<?= the_field('fr_hw_button_url'); ?>" class="bt-details"><?= the_field('fr_hw_button_text'); ?></a>
	</div>
</section>
<?php get_footer(); ?>