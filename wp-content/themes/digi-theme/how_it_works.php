<?php 
/*
     Template Name: How it works
*/
get_header(); 
require_once("templates/hero-block.php");
?>
<section class="hw-in">
    <div class="container">
		<div class="video-block">
			<div class="row">
				<div class="col-md-7">
					<?php 
						if($video = get_field("video")):
							echo $video; 
						endif;
					?>
				</div>
			</div>		
		</div>
		<p class="hw-in-g-title">Sign up with Manage Your Property in 5 easy steps:</p>
		<?php if( have_rows('hiw_in') ):
		$count = 1;
		while( have_rows('hiw_in') ) : the_row();
		$hwtitle = get_sub_field('hiw_in_title');
		$hwdesc = get_sub_field('hiw_in_description'); ?>
		<div class="hiw-box">
			<div class="row">
				<div class="col-md-1 col-2"><p class="hw-number"><?= $count; ?></p></div>
				<div class="col-md-11 col-10 m-auto">
					<p class="hw-description"><?= $hwtitle; ?></p>
					<p class="hw-in-d hw-block"><?= $hwdesc; ?></p>
				</div>
			</div>
		</div>
		<?php $count++;
		endwhile;
		else :
		endif; ?>
	</div>
</section>
<?php get_footer(); ?>