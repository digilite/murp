<?php
// ADD CUSTOM POST TYPE IF NEEDED

function custom_post_type_events() {
	$labels = [
		"name"               => _x("Testimonials", "post type general name"),
		"singular_name"      => _x("Testimonials", "post type singular name"),
		"add_new"            => _x("Add New", "Testimonials"),
		"add_new_item"       => __("Add New Testimonial"),
		"edit_item"          => __("Edit Testimonial"),
		"new_item"           => __("New Testimonial"),
		"all_items"          => __("All Testimonials"),
		"view_item"          => __("View Testimonial"),
		"search_items"       => __("Search Testimonial"),
		"not_found"          => __("No Testimonials found"),
		"not_found_in_trash" => __("No Testimonials found in the Trash"), 
		"parent_item_colon"  => "",
		"menu_name"          => "Testimonials"
	];

	$args = [
		"labels"        => $labels,
		"public"        => true,
		"menu_position" => 5,
		"supports"      => ["title", "editor", "thumbnail", "post-formats" ,"custom-fields"],
		"has_archive"   => true,
	];
	register_post_type("testimonials", $args);
}
add_action("init", "custom_post_type_events");
