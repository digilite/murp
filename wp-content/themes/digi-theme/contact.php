<?php 

/*
     Template Name: Contact
*/

get_header(); 


require_once("templates/hero-block.php");

?>

<section class="contact-in">
    <div class="container">
		<div class="row justify-content-between">
			<div class="col-md-7">
				<p class="c-get-in-touch">Get in touch with us to learn more about Manage Your Property.</p>
				<p class="off-hourse c-g"><span class="c-in">Office Hours:</span> Mon - Sun 9:00AM - 6:00PM</p>
				<p class="c-phone c-g"><span class="c-in">Phone:</span> 647-615-2884</p>
				<p class="c-email c-g"><span class="c-in">Email:</span> info@manageyourproperty.com</p>
				<p class="c-g c-in con-social">Connect with us on social media</p>
				<div class="social-icons c-social">
					<a href="https://www.facebook.com/Manage-Your-Property-111162394043969"><i class="fab fa-facebook-f"></i></a>
					<a href="https://www.instagram.com/manageyourproperty/"><i class="fab fa-instagram"></i></a>
					<a href="https://www.youtube.com/channel/UCh4Y7KrHsBAciZPuBKMgaNw?view_as=subscriber"><i class="fab fa-youtube"></i></a>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-1">
				<?php echo do_shortcode("[formidable id=1]"); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>