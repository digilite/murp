<?php 
/*
     Template Name: Pricing
*/
get_header(); 
require_once("templates/hero-block.php");
?>
<section class="pricing">
	<div class="pricing-tabs">
		<div class="container">
			<div class="row tabs">
				<div class="col-4 text-center tabs-item tabs-item-1 active">
					<p class="tabs-title">Condominium</p>
				</div>
				<div class="col-4 text-center tabs-item tabs-item-2">
					<p class="tabs-title">Single family home</p>
				</div>
				<div class="col-4 text-center tabs-item tabs-item-3">
					<p class="tabs-title">Multi residential up to 4 doors </p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<?php echo do_shortcode("[formidable id=8]"); ?>
	</div>

</section>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="exampleModalLongTitle">Send request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  		<?php echo do_shortcode('[formidable id=2]'); ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="exampleModalLongTitle">For 5+ units please contact us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	       <?php echo do_shortcode('[formidable id=3]'); ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalCenter3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h5 class="modal-title" id="exampleModalLongTitle">For 4+ doors please contact us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	       <?php echo do_shortcode('[formidable id=3]'); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>